<?php

namespace Symbiont\Utilizer\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface Groupable extends Utilizeable {

    public function scopeGroup(Builder $query, string $group): void;
    public function scopeGroups(Builder $query, array $groups): void;

}