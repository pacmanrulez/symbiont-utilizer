<?php

namespace Symbiont\Utilizer\Contracts;

use Illuminate\Database\Eloquent\Model;

interface Homeable extends Defaultable {

    public static function getHome(): Homeable|null;
    public function markAsHome(): bool;
    public function isHome(): bool;
}