<?php

namespace Symbiont\Utilizer\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface Sequenceable extends Utilizeable {

    public function scopeSequence(Builder $query): void;
    public function scopeSequenceDesc(Builder $query): void;

}