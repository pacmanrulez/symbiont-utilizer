<?php

namespace Symbiont\Utilizer\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface Defaultable extends Utilizeable {

    public function isDefault(): bool;
    public function markAsDefault(): bool;
    public function scopeDefaultet(Builder $query): void;

}