<?php

namespace Symbiont\Utilizer\Contracts;

interface Pageable extends Homeable, Publishable { }