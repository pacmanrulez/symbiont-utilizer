<?php

namespace Symbiont\Utilizer\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface Publishable extends Utilizeable {

    public function initializeHandlesPublishing(): void;

    public function scopePublished(Builder $query): void;
    public function scopeDrafts(Builder $query): void;
    public function scopeOffline(Builder $query): void;

    public function setPublished(): bool;
    public function setOffline(): bool;
    public function setDraft(): bool;

    public function isPublished(): bool;
    public function isOffline(): bool;
    public function isDraft(): bool;

}