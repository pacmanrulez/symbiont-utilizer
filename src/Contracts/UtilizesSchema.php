<?php

namespace Symbiont\Utilizer\Contracts;

use Symbiont\Utilizer\Pipes\Transportable;

interface UtilizesSchema {

    public function schema(Transportable $transporter): Transportable;

}