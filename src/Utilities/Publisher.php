<?php

namespace Symbiont\Utilizer\Utilities;

use Illuminate\Database\Schema\Blueprint;
use Symbiont\Utilizer\Casts\PublishedCast;
use Symbiont\Utilizer\Contracts\UtilizesSchema;
use Symbiont\Utilizer\Pipes\Transportable;

class Publisher extends BaseUtility implements UtilizesSchema {

    /**
     * Add publishing to table schema
     * @param Blueprint $table
     * return Blueprint
     */
    public function schema(Transportable $transporter): Transportable {
        $transporter->send->tinyInteger($transporter->model::ATTR_PUBLISHED)
            ->default(PublishedCast::Offline);
        $transporter->send->timestamp($transporter->model::ATTR_PUBLISHED_AT)
            ->nullable();

        return $transporter;
    }

}