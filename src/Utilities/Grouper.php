<?php

namespace Symbiont\Utilizer\Utilities;

use Illuminate\Database\Schema\Blueprint;

use Symbiont\Utilizer\Contracts\UtilizesSchema;
use Symbiont\Utilizer\Pipes\Transportable;

class Grouper extends BaseUtility implements UtilizesSchema {

    public static $TABLE_PROPERTY_LIMIT = 50;

    /**
     * Add publishing to table schema
     * @param Blueprint $table
     * @return Blueprint
     */
    public function schema(Transportable $transporter): Transportable {
        $transporter->send->string($transporter->model::ATTR_GROUP, static::$TABLE_PROPERTY_LIMIT)
            ->nullable();

        return $transporter;
    }

}