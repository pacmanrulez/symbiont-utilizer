<?php

namespace Symbiont\Utilizer\Utilities;

use Symbiont\Utilizer\Pipes\Transportable;

abstract class BaseUtility implements Utilizer {

    protected static array $resolvers = [];
    private static array $_resolvers = [];

    const METHOD_INIT_UTILIZER = 'initUtilizer';

    /**
     * Create instance, (new class)->method preferred!
     * @return static
     */
    public static function create() {
        return new static();
    }

    public function __construct() {
        if(method_exists($this, static::METHOD_INIT_UTILIZER)) {
            $this->{static::METHOD_INIT_UTILIZER}();
        }
    }

    /**
     * Handle utility
     * @param $transporter
     * @return mixed
     * @throws \Exception
     */
    public function handle(Transportable $transporter, \Closure $next): mixed {
        $resolved = $transporter;
        $resolvers = static::getResolvers();
        $resolver_key = $transporter->getTransportName();

        if(! is_string($resolver_key) || ! array_key_exists($resolver_key, $resolvers) )  {
            return $next($transporter);
        }

        $method = $resolvers[$resolver_key];

        if(is_string($method) && method_exists($this, $method)) {
            $resolved = call_user_func([$this, $method], $transporter);
        }
        elseif(is_callable($method)) {
            $resolved = \Closure::bind($method, $this)($transporter);
        }

        // not to be solved, pass on
        return $next($resolved);
    }

    /**
     * Get resolvers
     * @param bool $with_defaults
     * @return array|string[]
     */
    public static function getResolvers(bool $with_defaults = true): array {
        return $with_defaults ?
            array_merge(self::$_resolvers, static::$resolvers):
            static::$resolvers;
    }

    /**
     * Get default resolvers
     * @return array
     */
    public static function getDefaultResolvers() {
        return static::$_resolvers;
    }

    /**
     * Add resolvers.
     * @param array $resolvers
     * @return void
     */
    public static function addResolvers(array $resolvers): void {
        static::$resolvers = array_merge(static::$resolvers, $resolvers);
    }

    /**
     * Add default resolvers
     * @param array $resolvers
     * @return void
     */
    public static function addDefaultResolvers(array $resolvers): void {
        static::$_resolvers = array_merge(static::$_resolvers, $resolvers);
    }

    /**
     * Set default resolvers
     * @param array $resolvers
     * @return void
     */
    public static function setDefaultResolvers(array $resolvers): void {
        static::$_resolvers = $resolvers;
    }

}