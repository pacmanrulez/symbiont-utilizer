<?php

namespace Symbiont\Utilizer\Utilities;

use Illuminate\Database\Schema\Blueprint;
use Symbiont\Utilizer\Contracts\UtilizesSchema;
use Symbiont\Utilizer\Pipes\Transportable;

class Sequencer extends BaseUtility implements UtilizesSchema {

    /**
     * Add publishing to table schema
     * @param Blueprint $table
     * @return Blueprint
     */
    public function schema(Transportable $transporter): Transportable {
        $transporter->send->integer($transporter->model::ATTR_SEQUENCE)
            ->nullable();

        return $transporter;
    }

}