<?php

namespace Symbiont\Utilizer\Utilities;

use Symbiont\Utilizer\Pipes\Transportable;

interface Utilizer {

    public function handle(Transportable $transporter, \Closure $next): mixed;

}