<?php

namespace Symbiont\Utilizer\Utilities;

use Illuminate\Database\Schema\Blueprint;

use Symbiont\Utilizer\Casts\DefaultmentCast;
use Symbiont\Utilizer\Contracts\UtilizesSchema;
use Symbiont\Utilizer\Pipes\Transportable;

class Defaulter extends BaseUtility implements UtilizesSchema {

    /**
     * Add publishing to table schema
     * @param Blueprint $table
     * @return Blueprint
     */
    public function schema(Transportable $transporter): Transportable {
        $transporter->send->tinyInteger($transporter->model::ATTR_DEFAULT)
            ->default(DefaultmentCast::None);

        return $transporter;
    }

}