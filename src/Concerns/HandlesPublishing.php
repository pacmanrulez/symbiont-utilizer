<?php

namespace Symbiont\Utilizer\Concerns;

use Illuminate\Database\Eloquent\Builder;
use Symbiont\Utilizer\Casts\PublishedCast;
use Symbiont\Utilizer\Utilities\Publisher;

trait HandlesPublishing {

    use AddsPublishingStates;

    const ATTR_PUBLISHED = 'published';
    const ATTR_PUBLISHED_AT = 'published_at';

    public function initializeHandlesPublishing(): void {
        $this->casts = array_merge($this->casts, [
            self::ATTR_PUBLISHED => PublishedCast::class
        ]);
        $this->fillable = array_merge($this->fillable, [
            self::ATTR_PUBLISHED
        ]);
    }

    protected function whereScopedPublishQuery(Builder $query, PublishedCast $cast) {
        $query->where(self::ATTR_PUBLISHED, $cast->value);
    }

    /**
     * Update published status
     * @param $value
     * @return bool
     */
    protected function updatePublishedStatus(PublishedCast|int $state) {
        $value = is_int($state) ?
             $state : $state->value;

        return $this->update([
            self::ATTR_PUBLISHED => $value,
            self::ATTR_PUBLISHED_AT => $state === PublishedCast::Published
                ? now() : null
        ]);
    }

    public function scopePublished(Builder $query): void {
        $this->whereScopedPublishQuery($query,PublishedCast::Published);
    }

    public function scopeDrafts(Builder $query): void {
        $this->whereScopedPublishQuery($query,PublishedCast::Draft);
    }

    public function scopeOffline(Builder $query): void {
        $this->whereScopedPublishQuery($query,PublishedCast::Offline);
    }

    /**
     * @return bool
     */
    public function setPublished(): bool {
        return $this->updatePublishedStatus(PublishedCast::Published);
    }

    /**
     * @return bool
     */
    public function setOffline(): bool {
        return $this->updatePublishedStatus(PublishedCast::Offline);
    }

    /**
     * @return bool
     */
    public function setDraft(): bool {
        return $this->updatePublishedStatus(PublishedCast::Draft);
    }

    /**
     * @return bool
     */
    public function isPublished(): bool {
        return $this->{Publisher::$TABLE_PROPERTY_PUBLISHED} == PublishedCast::Published;
    }

    /**
     * @return bool
     */
    public function isOffline(): bool {
        return $this->{Publisher::$TABLE_PROPERTY_PUBLISHED} == PublishedCast::Offline;
    }

    /**
     * @return bool
     */
    public function isDraft(): bool {
        return $this->{Publisher::$TABLE_PROPERTY_PUBLISHED} == PublishedCast::Draft;
    }

}