<?php

namespace Symbiont\Utilizer\Concerns;

use Symbiont\Utilizer\Contracts\{
    Homeable, Publishable
};

trait HandlesHome {

    use HandlesDefaultment {
        HandlesDefaultment::markAsDefault as protected protectedMarkAsDefault;
    }

    /**
     * Get page designated as home
     * @return Homeable|null
     */
    public static function getHome(): Homeable|null {
        $query = self::defaultet();

        if(is_a(self::class, Publishable::class, true)) {
            $query->published();
        }

        return $query->first();
    }

    /**
     * Mark as home, only for published pages
     * @return bool
     */
    public function markAsHome() : bool {
        if($this instanceof Publishable
            && !$this->isPublished()) {
            return false;
        }

        return $this->protectedMarkAsDefault();
    }

    /**
     * We should avoid setting a homeable object to default without
     * the necessary checks to be marked as `home`.
     * @return bool
     */
    public function markAsDefault(): bool {
        return $this->markAsHome();
    }

    /**
     * Alias for isDefault
     * Is marked as home ('/')
     * @return bool
     */
    public function isHome(): bool {
        return $this->isDefault();
    }

}