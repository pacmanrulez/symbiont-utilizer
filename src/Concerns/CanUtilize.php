<?php

namespace Symbiont\Utilizer\Concerns;

use Symbiont\Utilizer\Utilizer;

trait CanUtilize {

    /**
     * Forward static to UtilizerFacade
     * @param mixed $send
     * @return mixed
     */
    public static function utilize($send, $method = null): mixed {
        $utilities = array_keys(array_intersect(
                Utilizer::$utilities,
                class_implements(static::class)));

        return Utilizer::utilize($utilities, static::class, $send, $method);
    }

}