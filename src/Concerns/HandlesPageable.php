<?php

namespace Symbiont\Utilizer\Concerns;

trait HandlesPageable {

    use HandlesHome,
        HandlesPublishing;

}