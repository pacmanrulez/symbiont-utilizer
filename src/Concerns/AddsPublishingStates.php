<?php

namespace Symbiont\Utilizer\Concerns;

use Symbiont\Utilizer\Casts\PublishedCast;

trait AddsPublishingStates {

    /**
     * @var int
     */
    public static $PUBLISHED_DEFAULT_STATE = PublishedCast::Draft;

}