<?php

namespace Symbiont\Utilizer\Concerns;

use Illuminate\Database\Eloquent\Builder;
use Symbiont\Core\Casts\DefaultmentCast;
use Symbiont\Core\Utilities\Sequencer;

trait HandlesSequences {

    const ATTR_SEQUENCE = 'sequence';

    public function initializeHandlesSequences() {
        $this->fillable = array_merge($this->fillable, [
            self::ATTR_SEQUENCE
        ]);
    }

    public function scopeSequence(Builder $query): void {
        $query->orderBy(self::ATTR_SEQUENCE);
    }

    public function scopeSequenceDesc(Builder $query): void {
        $query->orderBy(self::ATTR_SEQUENCE, 'desc');
    }

}