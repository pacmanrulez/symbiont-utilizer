<?php

namespace Symbiont\Utilizer\Concerns;

use Illuminate\Database\Eloquent\Builder;
use Symbiont\Utilizer\Utilities\Grouper;

trait HandlesGroups {

    const ATTR_GROUP = 'group';

    public function initializeHandlesGroups() {
        $this->fillable = array_merge($this->fillable, [
            self::ATTR_GROUP
        ]);
    }

    /**
     * Transform a group name to a "slug-like" string.
     * @param string $group
     * @return string
     */
    public function transformGroupName(string $group): string {
        return str($group)->lower()->slug('-')->toString();
    }

    /**
     * @param $value
     * @return \Illuminate\Support\Stringable|mixed
     */
    public function setGroupAttribute($value) {
        return $this->transformGroupName($value);
    }

    public function scopeGroup(Builder $query, string $group): void {
        $query->where(self::ATTR_GROUP, $this->transformGroupName($group));
    }

    public function scopeGroups(Builder $query, array $groups): void {
        $query->where(function($where) use ($groups) {
            foreach($groups as $index => $group) {
                $where->{$index > 0 ? 'orWhere' : 'where'}(self::ATTR_GROUP, $this->transformGroupName($group));
            }
        });
    }

}