<?php

namespace Symbiont\Utilizer\Concerns;

use Illuminate\Database\Eloquent\{
    Model, Builder
};
use Symbiont\Utilizer\{
    Casts\DefaultmentCast, Utilities\Defaulter
};

trait HandlesDefaultment {

    const ATTR_DEFAULT = 'dfault';

    public function initializeHandlesDefaultment(): void {
        // will default have a cast?
        $this->casts = array_merge($this->casts, [
            self::ATTR_DEFAULT => DefaultmentCast::class
        ]);

        $this->fillable = array_merge($this->fillable, [
            self::ATTR_DEFAULT
        ]);
    }

    public static function bootHandlesDefaultment() {
        // set all `other` defaults to false
        $callback = function(Model $model) {
            if(! $model->isDefault()) {
                return;
            }

            self::where(self::ATTR_ID, '!=', $model->getAttribute(self::ATTR_ID))
                ->update([
                    self::ATTR_DEFAULT => DefaultmentCast::None
                ]);
        };

        static::updated($callback);
        static::created($callback);
    }

    public function scopeDefaultet(Builder $query): void {
        $query->where(self::ATTR_DEFAULT, DefaultmentCast::Defaultet->value);
    }

    public function scopeNoneDefault(Builder $query): void {
        $query->where(self::ATTR_DEFAULT, DefaultmentCast::None->value);
    }

    // singular mutatable state
    public function isDefault(): bool {
        return $this->{self::ATTR_DEFAULT} == DefaultmentCast::Defaultet;
    }

    public function setDefault() {
        $this->{self::ATTR_DEFAULT} = DefaultmentCast::Defaultet;
    }

    public function markAsDefault(): bool {
        return self::update([
            self::ATTR_DEFAULT => DefaultmentCast::Defaultet
        ]);
    }

}