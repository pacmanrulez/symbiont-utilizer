<?php

namespace Symbiont\Utilizer;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

use Symbiont\Utilizer\Contracts\{
    Defaultable, Groupable, Publishable, Sequenceable
};
use Symbiont\Utilizer\Utilities\{
    BaseUtility, Defaulter, Grouper, Publisher, Sequencer
};
use Symbiont\Utilizer\Pipes\Transportable;
use Symbiont\Utilizer\Pipes\Traveler;

class UtilizerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('utilizer.php'),
            ], 'config');
        }

        $this->app->bind(Transportable::class, Traveler::class);

        // default utilities
        Utilizer::addUtilities([
            Defaulter::class => Defaultable::class,
            Publisher::class => Publishable::class,
            Sequencer::class => Sequenceable::class,
            Grouper::class => Groupable::class
        ]);

        // default resolvers
        BaseUtility::setDefaultResolvers([
            Blueprint::class => 'schema'
            // @todo: add factory resolver
        ]);
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'utilizer');

        // Register the main class to use with the facade
        $this->app->singleton('utilizer', function () {
            return new Utilizer;
        });
    }
}
