<?php

namespace Symbiont\Utilizer;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Symbiont\Utilizer\Skeleton\SkeletonClass
 */
class UtilizerFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'utilizer';
    }
}
