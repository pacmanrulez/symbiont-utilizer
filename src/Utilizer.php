<?php

namespace Symbiont\Utilizer;

use Illuminate\Pipeline\Pipeline;
use Symbiont\Utilizer\Pipes\Transportable;

final class Utilizer {

    /**
     * Defaults
     * @var string[UtilizerUtility::class, Utilizeable::class]
     */
    public static $utilities = [];

    public static function utilize(array $utilities, string $model, mixed $send, ?string $method = null) {
        return app(Pipeline::class)
            ->send(app(Transportable::class, [
                'model' => $model,
                'send' => $send,
                'method' => $method
            ]))
            ->through($utilities)
            ->then(fn($transporter)
                => $transporter->getValue());
    }

    public static function setUtilitiesMap(array $utilities): void {
        static::$utilities = $utilities;
    }

    /**
     * Return the utilities array
     * @return array|string
     */
    public static function getUtilitiesMap() {
        return static::$utilities;
    }


    /**
     * @param array $utilities
     * @return void
     */
    public static function addUtilities(array $utilities): void {
        static::$utilities = array_merge(static::$utilities, $utilities);
    }

    /**
     * @param $target
     * @param string $utilizer
     * @param string $contract
     * @return void
     */
    public static function replaceUtility($target, string $utilizer, string $contract) {
        if(array_key_exists($target, static::$utilities)) {
            unset(static::$utilities[$target]);
            static::$utilities[$utilizer] = $contract;
        }
    }

    /**
     * Get the Utilizer by Utilizable contract
     * @param string $class
     * @param bool $resolve
     * @return string|Utilities\Utilizer|null
     */
    public static function getUtility(string $class, bool $resolve = false): string|Utilities\Utilizer|null {
        if(! ($utilizer = static::$utilities[$class] ?? null) ) {
            return null;
        }

        return $resolve ? app($utilizer) : $utilizer;
    }

    /**
     * Get all utilities contracted with given class
     * @param string $class
     * @return array
     */
    public static function getUtilities(string $class): array {
        return array_keys(array_intersect(static::$utilities, class_implements($class)));
    }

    /**
     * Get the Utilizable contract
     * @param string $class
     * @return false|int|string
     */
    public static function getContract(string $class) {
        return array_search($class, static::$utilities, true);
    }

}
