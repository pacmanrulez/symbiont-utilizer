<?php

namespace Symbiont\Utilizer\Casts;

enum PublishedCast: int {

    case Offline = 0;
    case Published = 1;
    case Draft = 2;

    public static function fromName($name) {
        // 8.3 => PublishedCast::{$name} will be possible.
        return constant("static::$name");
    }

    public function getKey() {
        return match($this) {
            self::Offline => 'offline',
            self::Published => 'published',
            self::Draft => 'draft'
        };
    }

    public function getLabel() {
        return match($this) {
            self::Offline => 'Offline',
            self::Published => 'Publiziert',
            self::Draft => 'Entwurf'
        };
    }

    public function getIcon() {
        return match($this) {
            self::Offline => 'heroicon-o-clock',
            self::Published => 'heroicon-o-check-circle',
            self::Draft => 'heroicon-o-pencil'
        };
    }

    public function getColor() {
        return match($this) {
            self::Offline => 'warning',
            self::Published => 'success',
            self::Draft => 'info'
        };
    }
}