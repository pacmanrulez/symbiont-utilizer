<?php

namespace Symbiont\Utilizer\Pipes;

class Traveler implements Transportable {

    public readonly string $model;
    public readonly mixed $send;

    public readonly ?string $resolver_key;
    public readonly ?string $method;

    protected array $properties;

    /**
     * The target to retrieve the value from a bag, default null which will return $this->send property
     * @var string|null
     */
    protected ?string $target = null;

    public function __construct(string $model, mixed $send, ?string $method = null) {
        $this->properties = [];
        $this->model = $model;
        $this->send = $send;
        $this->resolver_key = is_object($send) ?
            get_class($send) : $send;

        if(! is_string($this->resolver_key)) {
            throw new \Exception('resolver key requires to be string, `' . gettype($this->resolver_key) . '` given');
        }

        $this->method = $method;
    }

    public function getModel(): string {
        return $this->model;
    }

    public function setTarget($target) {
        $this->target = $target;
        return $this;
    }

    public function getTarget() {
        return $this->target;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws \Exception
     * @return mixed
     */
    public function getBagValue(string $key): mixed {
        if(! array_key_exists($key, $this->properties)) {
            throw new \Exception('key ' . $key . ' not defined in transporter bag');
        }
        return $this->properties[$key];
    }


    public function getTransportName(): string {
        return $this->resolver_key;
    }

    /**
     * Get main return value from the transporter.
     * @return mixed
     * @throws \Exception
     */
    public function getValue() {
        return $this->target ?
            $this->getBagValue($this->target) :
            $this->send;
    }

    public function getProperties(): array {
        return $this->properties;
    }

    public function __set($property, $value) {
        $this->properties[$property] = $value;
    }

    public function __get($property) {
         return $this->properties[$property] ?? null;
    }

    public function __toString(): string {
        return json_encode($this->__toArray());
    }

    public function __toArray(): array {
        return [
            'send' => $this->send,
            'class' => $this->resolver_key,
            'method' => $this->method,
            'model' => $this->model
        ];
    }

}