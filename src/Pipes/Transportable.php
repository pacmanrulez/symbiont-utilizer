<?php

namespace Symbiont\Utilizer\Pipes;

interface Transportable {

    public function getModel(): string;

}