# Utilizer

!!! warning
    This package is `work in progress`! Ideas go fly as they fly by! This package will go through ongoing development,
    if not by change, of a proportional miniscule probability, I get a life.

This package is an attempt to create utilities to automate work for models depending on its contracts. The description 
might sound very wacky-dacky but that will change soon.

This package contains default Utility classes for publishing, set defaults and ordering (sequence) for Eloquent models.

Utility examples to be used with [Filaments](https://filamentphp.com/) [Tables](https://filamentphp.com/docs/3.x/forms/installation) and [Forms](https://filamentphp.com/docs/3.x/tables/installation)
will follow soon. 

Some examples with Livewire components will be included soon too.

## Installation

=== "composer.json"
    ```json
    "minimum-stability": "dev",
    "repositories": [
        { 
            "type": "vcs", 
            "url": "https://gitlab.com/pacmanrulez/symbiont-utilizer.git",
        }
    ],
    ```

=== "composer require"
    ```bash
    composer require symbiont/utilizer: "dev-main"
    ```

## Basics

Example model page with contracts.

```php title="example model"
<?php
use Symbiont\Utilizer\Contacts\{
    Publishable, Defaultable, Sequenceable
}
use Symbiont\Utilizer\Concerns\{
    HandlesPublishing, HandlesDefaultment, HandlesSequencing
}

class Page implements Publishable, Defaultable, Sequencable {
    use HandlesPublishing, HandlesDefaultment, HandlesSequencing;
    use CanUtilize;

    // ..
    
}
```

Both contracts Publishing and Defaultable need table fields, these can be added in the migration per migration file
for each model implementing these contracts.

### Default map

```php
<?php
    protected static $map = [
        Defaulter::class => Defaultable::class,
        Publisher::class => Publishable::class,
        Sequencer::class => Sequencable::class
    ];
```


- `Defaulter` utility class adds one table column `tinyinteger dfault` to the table `pages` if the `Defaultable` contract is implemented. 
    - Is used to set a page as default, e.g. `home`.
- `Publisher` utility class adds two table columns `tinyinteger published` and `timestamp published_at` to the table `pages` if the `Publishable` contract is implemented
    - Is used to publish a page by 3 different status: `offline`, `draft` and `published`.
- `Sequencer` utility class adds one table column `integer sequence` to the table `pages` if the `Sequenceable` contract is implemented
    - Is used to give a page an order (position, sequence).

## Example utility class

A utility class is based on a resolver property which contains the input class type referenced to a function.
Here the `Blueprint` class has been assigned to the method `schema`.

```php
<?php

namespace Symbiont\Utilizer\Utilities;

use Illuminate\Database\Schema\Blueprint;
use Symbiont\Utilizer\Casts\DefaultmentCast;

class Defaulter extends BaseUtility {

    protected array $resolvers = [
        Blueprint::class => 'schema'
    ];

    /**
     * Add publishing to table schema
     * @param Blueprint $table
     * @return Blueprint
     */
    public function schema(Blueprint $table): Blueprint {
        $table->tinyInteger('dfault')
            ->default(DefaultmentCast::None);

        return $table;
    }

}
```

If, for example, in the migration file, the `Blueprint $table` variable is passed an input to `Page::utilize`,
the utility class `Defaulter` would be called and run `Defaulter::schema` and add a column to the table. 

Migration file example

```php
<?php
use App\Models\Page;

return new class extends Migration {

    public function up() {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('title');

            $table = Page::utilize($table);

            $table->timestamps();
        });
    }
    
}
```

Because the `Page` model in this example implements `Publishable`, `Defaultable` and `Sequenceable` contracts, 4 columns in total
will be added to the table.